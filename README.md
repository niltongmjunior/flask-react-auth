# Authentication with Flask, React and Docker


[![pipeline status](https://gitlab.com/niltongmjunior/flask-react-auth/badges/master/pipeline.svg)](https://gitlab.com/niltongmjunior/flask-react-auth/commits/master)

This repository contains the contents for the [__Authentication with Flask, React, and Docker__](https://testdriven.io/courses/auth-flask-react/) course, which builds on the material from [__Test-Driven Development with Python, Flask, and Docker__](https://testdriven.io/courses/tdd-flask/) course from testdriven.io

The course covers:

- Python
- Flask (with RESTX, SQLAlchemy, Admin and Bcrypt)
- React (Formik, Modal, Yup and Router)
- Docker
- Postgres
- Gunicorn
- Swagger/OpenAPI
- TDD (with pytest and Coverage.py, and Jest)
- Linting (with flake8, black, isort and prettier/eslint)
- HTTPie
- CI/CD pipelines on Gitlab and deployment on Heroku

This app is currently running on [Heroku (click me to ping!)](https://flask-react-auth-nilton.herokuapp.com/)

API documentation can be found [here](https://flask-react-auth-nilton.herokuapp.com/doc)
